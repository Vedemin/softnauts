﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCtrl : MonoBehaviour {

    //This was a script used as a pre-test of to-add weapons, there however problems with accuracy that made me scrap the idea

    public Transform plane;
    public float roundsPerSecond;
    private float timeSinceRound;
    private bool readyToShoot = true;
    public int i;
    public Vector3 hitPoint;

	void Update () {

        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        
        RaycastHit hit;
        Physics.Raycast(transform.position, fwd, out hit);
        hitPoint = hit.point;

        //Debug.DrawRay(plane.position, fwd * 500, Color.green);

        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(transform.position, fwd, out hit))
                i++;    
        }
	}
}
