﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour {

    //This script is used to create a smootly following camera that enhances the experience of flying

    public float zoom, defaultZoom;
    public Transform target;
    public float smoothTime;
    private Vector3 velocity = Vector3.zero;
    void FixedUpdate()
    {
        transform.position = Vector3.SmoothDamp(transform.position, target.position, ref velocity, smoothTime);
        transform.rotation = target.rotation;
    }

    void Update()
    {
        //This is a zoom-in mechanics created to aim
        if (Input.GetMouseButton(1))
            Camera.main.fieldOfView = zoom;
        else
            Camera.main.fieldOfView = defaultZoom;
    }
}
