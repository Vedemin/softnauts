﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteerCube : MonoBehaviour {

    /* This is a script I created just to control the plane's speed and rotation.
    I decided to leave more physics to ControlCube script */

    public float rotationSlowX, rotationSlowY, rotationSlowZ;
    private float rotationX, rotationY, rotationZ, speedLimitX, speedLimitY;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate () {
        rotationY = Input.GetAxis("Mouse X") / Time.fixedDeltaTime / rotationSlowY;
        rotationX = Input.GetAxis("Mouse Y") / Time.fixedDeltaTime / rotationSlowX;
        rotationZ = Input.GetAxis("Horizontal") / Time.fixedDeltaTime / rotationSlowZ;

        speedLimitX = rotationSlowX / rb.velocity.magnitude * 20;
        speedLimitY = rotationSlowY / rb.velocity.magnitude * 20;


        if (rotationX > speedLimitX)
            rotationX = speedLimitX;
        else if (rotationX < -speedLimitX)
            rotationX = -speedLimitX;

        if (rotationY > speedLimitY)
            rotationY = speedLimitY;
        else if (rotationY < -speedLimitY)
            rotationY = -speedLimitY;

        rb.AddRelativeTorque(-rotationX, rotationY, -rotationZ);
	}
}
