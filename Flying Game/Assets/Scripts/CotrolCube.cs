﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CotrolCube : MonoBehaviour {

    /* In reality, this is a flying script I created inspired by Air Brawl.
    The mechanics are quite similar, but I decided to also use "realistic" lift forces. */

	private Rigidbody rb;
	public float throttlePower;
    public float lift;
    public float sideStop;

    [SerializeField]
    private float throttle, liftForce, forwardVelocity, sideVelocity, upVelocity;

    void Start () {
		rb = GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
	}
	
	void FixedUpdate () {
        throttle += Input.GetAxis ("Vertical") * throttlePower;
        //The trottle is set to a variable only modified when the thrust changes instead of turning it on and off

        if (throttle > 20)
            throttle = 20;
        else if (throttle < 0)
            throttle = 0;

        //I added opposing forces that stop the airplane when it turns
        forwardVelocity = transform.InverseTransformDirection(rb.velocity).z;
        sideVelocity = transform.InverseTransformDirection(rb.velocity).x;
        upVelocity = transform.InverseTransformDirection(rb.velocity).y;

        liftForce = lift * forwardVelocity;

        if (forwardVelocity < 50)
            rb.AddRelativeForce (Vector3.forward * throttle);

        rb.AddRelativeForce(Vector3.up * liftForce);

        if (throttle == 0 && forwardVelocity > 0)
            rb.AddRelativeForce(Vector3.forward * -throttlePower * 5);

        if (upVelocity > 3 || upVelocity < -3)
            rb.AddRelativeForce(Vector3.down * liftForce * upVelocity);

        rb.AddRelativeForce(Vector3.left * sideStop * sideVelocity);


    }
}