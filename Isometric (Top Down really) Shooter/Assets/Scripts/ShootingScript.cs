﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ShootingScript : MonoBehaviour {

    [SerializeField] Rigidbody bulletPrefab;
    [SerializeField] float roundsPerSecond = 1;
    [SerializeField] float bulletSpeed = 10f;

    [SerializeField] Transform slide;
    [SerializeField] Transform shootingPoint;
    [SerializeField] ParticleSystem flame;

    [SerializeField] GameObject light;

    private float timeUntilShot = 0f;
    private AudioSource audio;
    public AudioClip shot;
    private Vector3 slidePosition;

	void Start () {
        audio = GetComponent<AudioSource>();
	}
	
	void Update () {
        Vector3 origin = shootingPoint.position;

		if (Input.GetKeyDown(KeyCode.Mouse0) && timeUntilShot <= 0f)
        {
            slidePosition = slide.localPosition;
            Shoot(origin);
        }
        else if (timeUntilShot <= 0f)
        {
            slide.transform.localPosition = slidePosition;
        }
        else if (timeUntilShot > 0f)
        {
            timeUntilShot -= Time.deltaTime;
            slide.transform.Translate(0f, 0f, 1f * Time.deltaTime);
        }
	}

    void Shoot(Vector3 origin)
    {
        Rigidbody bullet;
        bullet = Instantiate(bulletPrefab, origin, transform.rotation) as Rigidbody;
        bullet.velocity = transform.TransformDirection(Vector3.forward * bulletSpeed);
        audio.pitch = Random.Range(0.9f, 1.1f);
        audio.PlayOneShot(shot);

        flame.Play();

        timeUntilShot = 1f / roundsPerSecond;

        slide.transform.Translate(0f, 0f, -0.3f);

        light.SetActive(true);
        StartCoroutine(LightDelay(0.1f));
    }

    IEnumerator LightDelay(float time)
    {
        yield return new WaitForSeconds(time);
        light.SetActive(false);
    }

    IEnumerator SlideDelay(float time)
    {
        yield return new WaitForSeconds(time);
        light.SetActive(false);
    }
}
