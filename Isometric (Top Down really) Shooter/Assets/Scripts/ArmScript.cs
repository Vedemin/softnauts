﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmScript : MonoBehaviour {

    //The script was meant to rotate the arm of player's character towards the mouse, so that the bullets would pass directly through mouse coursor's position

    RaycastHit hit;

	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            Vector3 dir = transform.position - hit.point;
            float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg + 180f;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
        }
    }
}
