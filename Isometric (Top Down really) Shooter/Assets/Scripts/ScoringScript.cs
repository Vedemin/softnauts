﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoringScript : MonoBehaviour {
    public int lampsDestroyed;
    private TextMeshProUGUI text;

    private void Start()
    {
        text = GetComponent<TextMeshProUGUI>();
        text.text = "Lamps Destroyed: " + lampsDestroyed;
    }

    public void ChangeLampAmount()
    {
        lampsDestroyed++;
        text.text = "Lamps Destroyed: " + lampsDestroyed;
    }

}
