﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDController : MonoBehaviour {

    [SerializeField] float movementSpeed = 3f;
    [SerializeField] Texture2D crosshair;
    private Rigidbody rigidbody;
    RaycastHit hit;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        Cursor.SetCursor(crosshair, Vector2.zero, CursorMode.Auto);
    }

    void Update()
    {
        Move();
        Rotate();
    }

    void Move()
    {
        Vector3 velocity = new Vector3 (Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical"));
        rigidbody.MovePosition(rigidbody.position + velocity * Time.fixedDeltaTime * movementSpeed);
    }

    void Rotate()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            Vector3 dir = rigidbody.position - hit.point;
            float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg + 180f;
            rigidbody.rotation = Quaternion.AngleAxis(angle, Vector3.up);
        }
    }
}
