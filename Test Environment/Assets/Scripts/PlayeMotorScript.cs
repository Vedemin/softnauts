﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerControllerScript))]

public class PlayeMotorScript : MonoBehaviour { //Unfortunately I misspelled the script name but as it doesn't have any effect on the project, I decided to leave it as the group liked the misspelled name

    /* I use different scripts for collecting input from keyboard and the scene and for executing the movements,
    this is caused by a need for clarity as well as a necessity for FixedUpdate() in case of Rigidbody physics */

    [SerializeField] float movementSpeed = 2.0f;

    private Rigidbody rigidbody;

    private float inputHorizontal;
    private float inputVertical;

    private Vector3 target;

	void Start () {
        rigidbody = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate () {
        Move();
	}

    private void Move()
    {
        Vector3 moveDirection = new Vector3(inputHorizontal, 0f, inputVertical);

        rigidbody.MovePosition(rigidbody.position + transform.TransformDirection(moveDirection.normalized * movementSpeed * Time.fixedDeltaTime));
    }

    public void GetInput(float inputHor, float inputVer)
    {
        inputHorizontal = inputHor;
        inputVertical = inputVer;
    }
}
