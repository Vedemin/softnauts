﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {
    [SerializeField] float timeToLive;

	void OnCollisionEnter (Collision collision) {
        if (collision.gameObject.tag == "Enemy") //I use an additional script (HPManagementScript) which allows me to control the amount of HP for both the player and the AI, it's quite universal in that regard
        {
            HPManagementScript hpscript = collision.gameObject.GetComponent<HPManagementScript>();

            hpscript.GetHitBro();
        }
        Destroy(gameObject);
	}

    void Start () { //Bullet is destroyed after a certain amount of time
        Destroy(gameObject, lifetime);
    }
}
