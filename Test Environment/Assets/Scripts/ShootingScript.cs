﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingScript : MonoBehaviour {

    //This script was made mostly for test purposes as I haven't made any rate of fire limitations. We simply needed a quick solution to test the basic bullet Rigidbody mechanics on the AI.

    [SerializeField] GameObject bulletPrefab;

    [SerializeField] float bulletSpeed = 2f;

	void Update () {
		if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
	}

   void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
        bullet.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, bulletSpeed);
    }
}
