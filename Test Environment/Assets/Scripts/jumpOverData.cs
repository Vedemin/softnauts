﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jumpOverData : MonoBehaviour {

    //In the future this script will be used in parkour mechanics when boarding over obstacles

    public float objectHeight;
    public Vector2 jumpOverVector;

}
