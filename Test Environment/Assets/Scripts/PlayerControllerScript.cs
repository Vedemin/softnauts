﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerScript : MonoBehaviour {

    private PlayeMotorScript motorScript;
    private Rigidbody rb;
    private BoxCollider jumpOverCollider;
    private bool jumpOver;
    private string parkourType;

    [SerializeField] float mouseSensitivityX = 1, mouseSensitivityY = 1;
    [SerializeField] float jumpHeight;
    public float jumpDistance;
    
    [SerializeField] Transform camera;

    private float yRotate = 0.0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        motorScript = GetComponent<PlayeMotorScript>();
        jumpOverCollider = GetComponent<BoxCollider>();
        jumpDistance = Mathf.Sqrt(2 * jumpHeight * -Physics.gravity.y); 
        //I want to set the exact units the player will jump, the script makes sure they are converted to required velocity for Rigidbody
    }

    void Update()
    {
        Move();
        Rotate();

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump(jumpDistance);
        }

        Debug.DrawRay(transform.position, Vector3.down); //A ray drawn to visually check if the player is grounded
    }

    private void Rotate()
    {
        transform.Rotate(0, Input.GetAxisRaw("Mouse X") * mouseSensitivityX, 0);

        yRotate -= Input.GetAxis("Mouse Y") * mouseSensitivityY * 35 * Time.deltaTime;
        yRotate = Mathf.Clamp(yRotate, -90.0f, 90.0f);
        camera.transform.localEulerAngles = new Vector3(yRotate, 0.0f, 0.0f);
    }

    private void Move() //This method collects the keyboard input and is called on each frame
    {
        float moveHorizontally = Input.GetAxisRaw("Horizontal");
        float moveVertically = Input.GetAxisRaw("Vertical");

        motorScript.GetInput(moveHorizontally, moveVertically);
    }

    private void Jump (float jumpDistance)
    {
        if(Physics.Raycast(transform.position, Vector3.down, 1.1f)) { //Code created for future parkour mechanics
            switch (parkourType)
            {
                case "jump_over":
                    JumpOverTheObject();
                    break;
                case "jump":
                    rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y + jumpDistance, rb.velocity.z);
                    break;
                default:
                    rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y + jumpDistance, rb.velocity.z);
                    break;
            }
        }
    }

    private void JumpOverTheObject()
    {
        //Not in use until the animator learns how to do such animations
    }

    private void OnTriggerEnter(Collider other) //I decided to use trigger to check the object in front of the player (I placed a box collider there and edited it to my needs)
    {
        if (other.tag == "jumpOver")
        {
            parkourType = "jump_over";
        }
        else if (other.bounds.size.y > 1.2f && other.bounds.size.y < 2.5f)
        {
            if (other.bounds.size.y < 2f)
                parkourType = "pull_up";
            else
            {
                parkourType = "jump_and_pull_up";
            }
        }
        else
        {
            parkourType = "jump";
        }
    }

    private void OnTriggerExit(Collider other)
    {
        parkourType = "jump"; //It makes sure that every time the player is no longer in front of a parkour-interactive object, he 
    }
}
