﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPManagementScript : MonoBehaviour {

    public int healthAmount; //I've set the health amount as a public variable in case I ever want to modify it by a (healing) script

    public void GetHitBro()
    {
        healthAmount--;

        if (healthAmount <= 0)
        {
            Destroy(gameObject);
        }
    }
}
