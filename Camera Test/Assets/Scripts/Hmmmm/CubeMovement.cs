﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeMovement : MonoBehaviour {

	public Quaternion rotation = Quaternion.identity;
	public Transform target;
	public float speed = 10f;
	private int rotationOffset = 0;

	void Update () {

		float rotationDirection = target.eulerAngles.y;

		if (Input.GetAxisRaw ("Horizontal") > 0 && Input.GetAxisRaw ("Vertical") > 0) {
			rotationOffset = 45;
		} else if (Input.GetAxisRaw ("Horizontal") < 0 && Input.GetAxisRaw ("Vertical") > 0) {
			rotationOffset = -45;
		} else if (Input.GetAxisRaw ("Horizontal") < 0 && Input.GetAxisRaw ("Vertical") < 0) {
			rotationOffset = -135;
		} else if (Input.GetAxisRaw ("Horizontal") > 0 && Input.GetAxisRaw ("Vertical") < 0) {
			rotationOffset = 135;
		} else if (Input.GetAxisRaw ("Horizontal") < 0 && Input.GetAxisRaw ("Vertical") < 0) {
			rotationOffset = -135;
		} else if (Input.GetAxisRaw ("Horizontal") == 0 && Input.GetAxisRaw ("Vertical") > 0) {
			rotationOffset = 0;
		} else if (Input.GetAxisRaw ("Horizontal") > 0 && Input.GetAxisRaw ("Vertical") == 0) {
			rotationOffset = 90;
		} else if (Input.GetAxisRaw ("Horizontal") == 0 && Input.GetAxisRaw ("Vertical") < 0) {
			rotationOffset = 180;
		} else if (Input.GetAxisRaw ("Horizontal") < 0 && Input.GetAxisRaw ("Vertical") == 0) {
			rotationOffset = -90;
		}

		if (Input.GetAxisRaw ("Horizontal") != 0|| Input.GetAxisRaw ("Vertical") != 0) {

			transform.localEulerAngles = new Vector3 (0, rotationDirection + rotationOffset, 0);
			transform.Translate (Vector3.forward * Time.deltaTime * speed);
		}

	}
}