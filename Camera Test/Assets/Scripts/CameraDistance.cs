﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDistance : MonoBehaviour {

    /* This is a script created to control the camera for 3rd person characters */

    public Transform origin;
    public float cameraDistance;
    private Vector3 rayCastOrigin;
    private Vector3 rayCastDirection;
    public float baseDistance;
    public float cameraMinus;

	void Start () {
        transform.position = new Vector3(0, 0, -cameraDistance);
        
    }

    void Update () { 
        rayCastOrigin = origin.position;
        rayCastDirection = origin.position - transform.position;
        rayCastDirection = rayCastDirection.normalized;
        RaycastHit hit;

        //I simply check where if there are any walls between the camera and the target, then react accordingly
		if (Physics.Raycast(rayCastOrigin, -rayCastDirection, out hit, baseDistance + 1)) 
        {
            if (hit.distance < baseDistance)
            {
                cameraDistance = hit.distance - cameraMinus;
            }
        }
        else
        {
            cameraDistance = baseDistance;
        }

        transform.localPosition = new Vector3(0, 0, -cameraDistance);
    }
}